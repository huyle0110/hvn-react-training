import React from 'react';
import { render } from "react-dom";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import createStore from "./store";
import 'bootstrap/scss/bootstrap.scss';

const { store, history } = createStore();

const renderContainer = Component => {
  const root = document.getElementById("root");

  const Application = (
    <Provider store={store}> {/* config su dung Redux trong React */}
      <Router history={history}> {/* config su dung React Router trong React */}
        <Component />
      </Router>
    </Provider>
  );

  // render component
  render(Application, root);
};

renderContainer(App);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
