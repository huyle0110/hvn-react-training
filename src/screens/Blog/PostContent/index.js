import React, { useEffect } from "react";
import {useDispatch, useSelector} from "react-redux";
import { Button } from "reactstrap";

import { requestPostStart, requestDeletePost } from "../../../actions";

const PostContent = ({ location }) => {
  const { pathname } = location;
  const postId = Number(pathname.replace("/blog/", ""));

  const { data, message } = useSelector(state => {
    return {
      data: state.Post.data,
      message: state.Post.message
    }
  });

  const removePost = (postId) => {
    dispatch(requestDeletePost(postId));
  };

  // dung dispatch de goi Redux action
  const dispatch = useDispatch();
  useEffect(() => {
    // goi den Redux action de lay data tu mock data hoac API
    dispatch(requestPostStart(postId));
  }, [dispatch, postId]);

  if (message !== null) {
    return <h4>{message}</h4>;
  }

  return (
    <>
      <h1>{data.title}</h1>
      <p>{data.body}</p>
      <p>
        <Button color="danger" onClick={() => removePost(data.id)}>Remove this post</Button>
      </p>
    </>
  );
};

export default PostContent;
